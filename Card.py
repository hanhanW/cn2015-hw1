from Tools import *

class Card():
    def __init__(self):
        self.cards = []
        self.lose = False
    def reset(self):
        self.cards = [getCard(), getCard()]
        self.lose = False
    def addCard(self):
        self.cards.append(getCard())
        if sum(self.cards) < 0:
            self.lose = True
    def canSplit(self):
        return len(self.cards) == 2 and value(self.cards[0]) == value(self.cards[1])
    def split(self):
        r = Card()
        r.cards = [self.cards[-1], getCard()]
        self.cards[-1] = getCard()
        return r
    def sum(self):
        return sum(self.cards)

