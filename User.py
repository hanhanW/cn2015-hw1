import threading
import thread, time, select
from Tools import *
from Card import *


class User(threading.Thread):
    def  __init__(self, conn):
        super(User, self).__init__()
        self.conn = conn
        self.money = 5000
        self.deal = 0
        self.cards = []
    def resetCard(self):
        c = Card()
        c.reset()
        self.cards = [c]
    def askMoney(self, sec):
        sendline(self.conn, 'askMoney ' + str(self.money))
        ret = recvline(self.conn, sec)
        self.deal = int(ret) if ret.isdigit() else self.money
        print 'user bet ' + str(self.deal)
    def askCmd(self, data, sec):
        sendline(self.conn, 'askCmd')
        sendJson(self.conn, data)
        ret = recvline(self.conn, sec)
        return ret
    def tellLose(self):
        sendline(self.conn, 'tellLose')
    def tellShow(self, data):
        sendline(self.conn, 'tellShow')
        sendJson(self.conn, data)
    def tellTimeout(self):
        sendline(self.conn, 'timeout')
