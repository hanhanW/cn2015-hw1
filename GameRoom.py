import threading, thread, time
import random
from User import *
from Env import *
from Tools import *
from Card import *
from config import *

class GuessRoom(threading.Thread):
    def  __init__(self, users, serv):
        super(GuessRoom, self).__init__()
        self.users = users
        self.serv = serv
    def genNumber(self):
        ret = ''
        while len(ret) < 4:
            num = str(random.randint(0,9))
            if num not in ret:
                ret += num
        self.answer = ret
    def guess(self, s):
        A = B = 0
        for i in range(len(s)):
            if s[i] == self.answer[i]:
                A = A + 1
            elif s[i] in self.answer:
                B = B + 1
        return str(A) + 'A' + str(B) + 'B'
    def run(self):
        for it in self.users:
            sendline(it.conn, 'Welcome to Guess Number Room!')
        self.genNumber()
        print 'The answer is : \033[92m' + self.answer + '\033[0m'
        win = []
        bye = []
        for i in range(10):
            for it in self.users:
                if it in bye: continue
                sendline(it.conn, 'askGuess')
                g = recvline(it.conn, thinkingTimeLimit)
                if len(g) != 4:
                    bye.append(it)
                    sendline(it.conn, 'end')
                    continue
                print 'guess ' + g
                ret = self.guess(g)
                if ret == '4A0B':
                    sendline(it.conn, 'Correct!')
                    win.append(it)
                else:
                    sendline(it.conn, ret)
                print 'result : ' + ret
            if (len(win) > 0): break
        time.sleep(1)
        print win
        print 'bye ' + str(len(bye)) + ' people'
        for it in self.users:
            if it not in bye:
                print 'send bye'
                sendline(it.conn, 'end')
            print 'send'
            sendline(it.conn, 'The game is over!')
            sendline(it.conn, 'There are ' + str(len(win)) + ' people won the game!')
            t = threading.Thread(target=self.serv.addConnection, args=(it,))
            t.daemon = True
            t.start()

class BjackRoom(threading.Thread):
    def  __init__(self, users, serv):
        super(BjackRoom, self).__init__()
        self.users = users
        self.serv = serv
    def run(self):
        for it in self.users:
            sendline(it.conn, 'Welcome to Black Jack Room!')
        print 'Wait for users dealing...'
        td = []
        for it in self.users:
            it.resetCard()
            print it.cards
            t = threading.Thread(target=it.askMoney, args=(betTimeLimit,))
            t.start()
            td.append(t)
        for it in td:
            it.join()
        servCard = Card()
        servCard.reset()
        uid = 0
        for it in self.users:
            cid = 0
            for c in it.cards:
                while True:
                    tmp = 0
                    for i in self.users:
                        data = getData(tmp, uid, cid, servCard, self.users)
                        i.tellShow(data)
                        tmp += 1
                    data = getData(uid, uid, cid, servCard, self.users)
                    cmd = it.askCmd(data, thinkingTimeLimit)
                    if cmd == 'split':
                        if len(it.cards) >= 4 or not c.canSplit(): c.lose = True
                        else: it.cards.append(c.split())
                    if cmd == 'hit': c.addCard()
                    if c.lose: it.tellLose()
                    if cmd == '':
                        c.lose = True
                        it.tellTimeout()
                    if cmd == 'stand' or c.lose: break
                cid += 1
            uid += 1
        while servCard.sum() < 17 and servCard.sum() != -1:
            servCard.addCard()
            tmp = 0
            for i in self.users:
                data = getData(tmp, uid, cid, servCard, self.users)
                i.tellShow(data)
                tmp += 1
        print 'End Game'
        for it in self.users:
            sendline(it.conn, 'end')
            sendline(it.conn, 'Game over')
            money = 0
            for i in it.cards:
                if i.lose or servCard.sum() > i.sum(): money -= 1
                elif servCard.sum() < i.sum(): money += 1
            money *= it.deal
            if money < 0: msg = ' You losed \033[95m' + str(-money) + '\033[0m dollars.'
            else: msg = 'You earned \033[93m' + str(money) + '\033[0m dollars.'
            it.money += money
            print it.money
            sendline(it.conn, msg)
            t = threading.Thread(target=self.serv.addConnection, args=(it,))
            t.daemon = True
            t.start()


        
