#!/usr/bin/env python2
import os
import argparse
import sys
import socket
import time
from Tools import *

ascArt = {}

def readAsciiArt():
    with open('number.txt', 'r') as f:
        for i in range(10):
            s = ''
            for j in range(6):
                s += f.readline()
            ascArt[str(i)] = s
        s = ''
        for j in range(6):
            s += f.readline()
        ascArt['A'] = s
        s = ''
        for j in range(6):
            s += f.readline()
        ascArt['B'] = s

def printGuessResult(x):
    s = ['']*6
    for i in x:
        tmp = ascArt[i].split('\n')
        for j in range(6):
            s[j] += tmp[j] + ' '
            if not i.isdigit():
                s[j] += '   '
    for i in range(6):
        print s[i]

def printWelcome():
    os.system('clear')
    print '''
 __    __    ___  _        __   ___   ___ ___    ___ 
|  |__|  |  /  _]| |      /  ] /   \ |   |   |  /  _]
|  |  |  | /  [_ | |     /  / |     || _   _ | /  [_ 
|  |  |  ||    _]| |___ /  /  |  O  ||  \_/  ||    _]
|  `  '  ||   [_ |     /   \_ |     ||   |   ||   [_ 
 \      / |     ||     \     ||     ||   |   ||     |
  \_/\_/  |_____||_____|\____| \___/ |___|___||_____|
                                                     
'''

def printBye():
    os.system('clear')
    print '''
  ____   ___    ___   ___    ____   __ __    ___ 
 /    | /   \  /   \ |   \  |    \ |  |  |  /  _]
|   __||     ||     ||    \ |  o  )|  |  | /  [_ 
|  |  ||  O  ||  O  ||  D  ||     ||  ~  ||    _]
|  |_ ||     ||     ||     ||  O  ||___, ||   [_ 
|     ||     ||     ||     ||     ||     ||     |
|___,_| \___/  \___/ |_____||_____||____/ |_____|
'''

def printWelcomeGuessRoom():
    os.system('clear')
    print '''
  ____    ___  __ __  _____ _____     ____   ___    ___   ___ ___ 
 /    |  /  _]|  |  |/ ___// ___/    |    \ /   \  /   \ |   |   |
|   __| /  [_ |  |  (   \_(   \_     |  D  )     ||     || _   _ |
|  |  ||    _]|  |  |\__  |\__  |    |    /|  O  ||  O  ||  \_/  |
|  |_ ||   [_ |  :  |/  \ |/  \ |    |    \|     ||     ||   |   |
|     ||     ||     |\    |\    |    |  .  \     ||     ||   |   |
|___,_||_____| \__,_| \___| \___|    |__|\_|\___/  \___/ |___|___|


'''

def printWelcomeBlackJackRoom():
    os.system('clear')
    print '''
 ______   _        _______  _______  _       _________ _______  _______  _       
(  ___ \\ ( \\      (  ___  )(  ____ \\| \\    /\\\\__    _/(  ___  )(  ____ \\| \\    /\\
| (   ) )| (      | (   ) || (    \\/|  \\  / /   )  (  | (   ) || (    \\/|  \\  / /
| (__/ / | |      | (___) || |      |  (_/ /    |  |  | (___) || |      |  (_/ / 
|  __ (  | |      |  ___  || |      |   _ (     |  |  |  ___  || |      |   _ (  
| (  \\ \\ | |      | (   ) || |      |  ( \\ \\    |  |  | (   ) || |      |  ( \\ \\ 
| )___) )| (____/\\| )   ( || (____/\\|  /  \\ \\|\\_)  )  | )   ( || (____/\\|  /  \\ \\
|/ \\___/ (_______/|/     \\|(_______/|_/    \\/(____/   |/     \\|(_______/|_/    \\/

'''                                                                                 

def printResult(win):
    os.system('clear')
    if win:
        print '''
 _______  _______  _        _______  _______  _______ _________ _______ 
(  ____ \\(  ___  )( (    /|(  ____ \\(  ____ )(  ___  )\\__   __/(  ____ \\
| (    \\/| (   ) ||  \\  ( || (    \\/| (    )|| (   ) |   ) (   | (    \\/
| |      | |   | ||   \\ | || |      | (____)|| (___) |   | |   | (_____ 
| |      | |   | || (\\ \\) || | ____ |     __)|  ___  |   | |   (_____  )
| |      | |   | || | \\   || | \\_  )| (\\ (   | (   ) |   | |         ) |
| (____/\\| (___) || )  \\  || (___) || ) \\ \\__| )   ( |   | |   /\\____) |
(_______/(_______)|/    )_)(_______)|/   \\__/|/     \\|   )_(   \\_______)
                                                                        
              _______                     _________ _          _        
    |\\     /|(  ___  )|\\     /|  |\\     /|\\__   __/( (    /|  ( )       
    ( \\   / )| (   ) || )   ( |  | )   ( |   ) (   |  \\  ( |  | |       
     \\ (_) / | |   | || |   | |  | | _ | |   | |   |   \\ | |  | |       
      \\   /  | |   | || |   | |  | |( )| |   | |   | (\\ \\) |  | |       
       ) (   | |   | || |   | |  | || || |   | |   | | \\   |  (_)       
       | |   | (___) || (___) |  | () () |___) (___| )  \\  |   _        
       \\_/   (_______)(_______)  (_______)\\_______/|/    )_)  (_)       
                                                                        '''
    else:
        print '''
          _______             _______  _______  _______ 
|\\     /|(  ___  )|\\     /|  (  ___  )(  ____ )(  ____ \\
( \\   / )| (   ) || )   ( |  | (   ) || (    )|| (    \\/
 \\ (_) / | |   | || |   | |  | (___) || (____)|| (__    
  \\   /  | |   | || |   | |  |  ___  ||     __)|  __)   
   ) (   | |   | || |   | |  | (   ) || (\\ (   | (      
   | |   | (___) || (___) |  | )   ( || ) \\ \\__| (____/\\
   \\_/   (_______)(_______)  |/     \\||/   \\__/(_______/
                                                        
     _        _______  _______  _______  _______        
    ( \\      (  ___  )(  ____ \\(  ____ \\(  ____ )       
    | (      | (   ) || (    \\/| (    \\/| (    )|       
    | |      | |   | || (_____ | (__    | (____)|       
    | |      | |   | |(_____  )|  __)   |     __)       
    | |      | |   | |      ) || (      | (\\ (          
    | (____/\\| (___) |/\\____) || (____/\\| ) \\ \\__       
    (_______/(_______)\\_______)(_______/|/   \\__/       
                                                        
'''

def printBomb():
    os.system('clear')
    print '''
 .----------------.  .----------------.  .----------------.  .----------------.  .----------------. 
| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
| |   ______     | || |     ____     | || | ____    ____ | || |   ______     | || |              | |
| |  |_   _ \    | || |   .'    `.   | || ||_   \  /   _|| || |  |_   _ \    | || |      _       | |
| |    | |_) |   | || |  /  .--.  \  | || |  |   \/   |  | || |    | |_) |   | || |     | |      | |
| |    |  __'.   | || |  | |    | |  | || |  | |\  /| |  | || |    |  __'.   | || |     | |      | |
| |   _| |__) |  | || |  \  `--'  /  | || | _| |_\/_| |_ | || |   _| |__) |  | || |     | |      | |
| |  |_______/   | || |   `.____.'   | || ||_____||_____|| || |  |_______/   | || |     |_|      | |
| |              | || |              | || |              | || |              | || |     (_)      | |
| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------' 

'''

def printTimeout():
    os.system('clear')
    print '''
  _____                __  __  U _____ u U  ___ u   _   _  _____   
 |_ " _|     ___     U|' \\/ '|u\\| ___"|/  \\/"_ \\/U |"|u| ||_ " _|  
   | |      |_"_|    \\| |\\/| |/ |  _|"    | | | | \\| |\\| |  | |    
  /| |\\      | |      | |  | |  | |___.-,_| |_| |  | |_| | /| |\\   
 u |_|U    U/| |\\u    |_|  |_|  |_____|\\_)-\\___/  <<\\___/ u |_|U   
 _// \\\\_.-,_|___|_,-.<<,-,,-.   <<   >>     \\\\   (__) )(  _// \\\\_  
(__) (__)\\_)-' '-(_/  (./  \\.) (__) (__)   (__)      (__)(__) (__) 
'''

def lobby(conn):
    os.system('clear')
    printWelcome()
    failTime = 0
    while True:
        print 'Please choose a game to play'
        print 'Enter 1: Guess Number'
        print 'Enter 2: Blackjack'
        print 'Enter q: for quit'
        cmd = raw_input('').strip()
        if cmd == '1':
            guessGame(conn)
            failTime = 0
            break
        elif cmd == '2':
            bjackGame(conn)
            failTime = 0
            break
        elif cmd == 'q':
            printBye()
            sys.exit()
        else:
            print 'Invalid Input'
            failTime += 1
            if failTime > 10:
                print 'Failed too much, close the program.'
                break

def validGuess(x):
    if len(x) != 4: return False
    for i in x:
        if x.count(i) != 1: return False
    return x.isdigit()

def requestGuess(rd):
    while True:
        print 'Round ' + str(rd)
        print 'Please guess a number with length 4 (e.g. 0842)'
        num = raw_input('').strip()
        if validGuess(num): return num
        else: print 'Invalid input, please input again!'

def guessGame(conn):
    sendline(conn, '1')
    printWelcomeGuessRoom()
    print recvline(conn, 10000)
    recvline(conn, 10000)
    print 'Round limit: 10'
    i = 0
    win = False
    while True:
        i = i + 1
        cmd = recvline(conn, 10000)
        if cmd != 'askGuess': break
        num = requestGuess(i)
        sendline(conn, num)
        ret = recvline(conn, 10000)
        if ret == 'Correct!': win = True
        elif ret == 'end':
            print 'Timeout!!'
            break
        else:
            print 'The result is '
            printGuessResult(ret)
    printResult(win)
    print recvline(conn, 10000)
    print recvline(conn, 10000)
    print '\n'*3
    raw_input('Press ENTER to continue ...')

def requestMoney(own):
    os.system('clear')
    while True:
        print 'You have \033[95m' + str(own) + '\033[0m dollars.'
        print 'Please input the number you want to bet.'
        money = raw_input('')
        if money.isdigit(): return money
        else: print 'Invalid input'

def printCard(x):
    s = sum(x)
    if s == -1: print '[ Bomb ]', 
    else: print '[ ' + str(min(21, s)) + ' ]',
    print ' '.join(map(str,x))

def showStatus(data):
    os.system('clear')
    myID, uid, cid, servCard, user = data
    n = len(user)
    print 'Dealer:'
    printCard(servCard)
    print '======================================================='
    for i in range(n):
        if i == myID: print '\033[1;33m'
        if i == uid:
            m = len(user[i])
            for j in range(m):
                if cid == j: print '>>>>',
                printCard(user[i][j])
        else:
            for card in user[i]:
                printCard(card)
        if i == myID: print '\033[0m'
        print '======================================================='

def canSplit(c):
    return len(c) == 2 and value(c[0]) == value(c[1])

def requestCmd(c):
    while True:
        print 'Please input your action'
        print 'Enter 1: hit'
        print 'Enter 2: stand'
        if canSplit(c):
            print 'Enter 3: split'
        ret = raw_input('')
        if ret == '1': return 'hit'
        if ret == '2': return 'stand'
        if ret == '3' and canSplit(c): return 'split'
        print 'Invalid input, try again ...'
def bjackGame(conn):
    sendline(conn, '2')
    printWelcomeBlackJackRoom()
    print recvline(conn, 10000)
    recvline(conn, 10000)
    betMoney = 0
    while True:
        cmd = recvline(conn, 10000)
        print '*** cmd = ' + cmd
        if 'askMoney' in cmd:
            betMoney = requestMoney(cmd.split()[-1])
            sendline(conn, str(betMoney))
            print 'Wait for other people...'
        elif cmd == 'askCmd':
            data = recvJson(conn)
            showStatus(data)
            sendline(conn, requestCmd(data[-1][int(data[0])][int(data[2])]))
        elif cmd == 'tellShow':
            data = recvJson(conn)
            showStatus(data)
        elif cmd == 'tellLose':
            printBomb()
            raw_input('Press ENTER to continue ...')
        elif cmd == 'end':
            break
        elif cmd == 'timeout':
            printTimeout()
        else:
            break
            print 'known command'
            print cmd
    print recvline(conn, 10000)
    print recvline(conn, 10000)
    raw_input('Press ENTER to continue ...')

def run(host, port):
    print 'start'
    conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 6)
    conn.connect((host, port))
    print 'Connect ' + host + ' ' + str(port)
    while True: lobby(conn)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, default=57122)
    return parser.parse_args()

if __name__ == "__main__":
    readAsciiArt()
    args = parse_args()
    run('127.0.0.1', args.port)
