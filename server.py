#!/usr/bin/env python2
import argparse
import socket
import sys
import select
import time
from GameRoom import *
from User import *
from Env import *

def runSocket(host, port):
    sver = Server(host, port)
    sver.run()

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, default=57122)
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    runSocket('127.0.0.1', args.port)
