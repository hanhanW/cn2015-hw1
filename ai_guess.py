#!/usr/bin/env python2
import argparse
import sys
import socket
import random
import time
from Tools import *
from config import *
from client import *

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, default=57122)
    return parser.parse_args()

def guess(a, b):
    A = 0
    B = 0
    for i in range(len(a)):
        if a[i] == b[i]:
            A = A + 1
        elif a[i] in b:
            B = B + 1
    return str(A) + 'A' + str(B) + 'B'

def filterSet(s1, num, ret):
    s2 = []
    for it in s1:
        if guess(it, num) == ret:
            s2.append(it)
    return s2

args = parse_args()

conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 6)
conn.connect(('127.0.0.1', args.port))
sendline(conn, '1')
print recvline(conn, 10000),
print recvline(conn, 10000),

s = []
for i in range(10):
    for j in range(10):
        if i == j:
            continue
        for k in range(10):
            if i == k or j == k:
                continue
            for l in range(10):
                if i == l or j == l or k == l:
                    continue
                s.append(str(i)+str(j)+str(k)+str(l))
win = False
readAsciiArt()
printWelcomeGuessRoom()
while True:
    cmd = recvline(conn, 10000)
    if cmd == 'end': break
    x = random.choice(s)
    print 'guess ' + x
    sendline(conn, x)
    ret = recvline(conn, 10000).strip()
    print ret
    if ret == 'Correct!': win = True
    else: printGuessResult(ret)
    s = filterSet(s, x, ret)
    time.sleep(aiWaitTime)
print 'Win' if win else 'Lose'
print recvline(conn, 10000)
print recvline(conn, 10000)
sendline(conn, 'q')
