#!/usr/bin/env python2
import argparse
import sys
import socket
import random
import time
from Tools import *
from client import *
from config import *

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--port', type=int, default=57122)
    return parser.parse_args()
def requestAICmd(c):
    if sum(c) < 17: return 'hit'
    return 'stand'

args = parse_args()

conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 6)
conn.connect(('127.0.0.1', args.port))
sendline(conn, '2')

printWelcomeBlackJackRoom()
print recvline(conn, 10000)
recvline(conn, 10000)
betMoney = 7122
while True:
    cmd = recvline(conn, 10000)
    print '*** cmd = ' + cmd
    if 'askMoney' in cmd:
        sendline(conn, str(betMoney))
        print 'Wait for other people...'
    elif cmd == 'askCmd':
        data = recvJson(conn)
        showStatus(data)
        sendline(conn, requestAICmd(data[-1][int(data[0])][int(data[2])]))
    elif cmd == 'tellShow':
        data = recvJson(conn)
        showStatus(data)
    elif cmd == 'tellLose':
        printBomb()
        raw_input('Press ENTER to continue ...')
    elif cmd == 'end':
        break
    elif cmd == 'timeout':
        printTimeout()
    else:
        break
        print 'known command'
        print cmd
    time.sleep(aiWaitTime)
print recvline(conn, 10000)
print recvline(conn, 10000)
raw_input('Press ENTER to continue ...')
sendline(conn, 'q')
