import socket
import sys
import select
import time
import threading
import GameRoom
import User
from Tools import *
from config import *

class Server:
    def  __init__(self, host, port):
        self.guessLock = threading.RLock()
        self.bjackLock = threading.RLock()
        self.guessRoom = []
        self.bjackRoom = []
        self.host = host
        self.port = port
    def add2GuessRoom(self, user):
        self.guessLock.acquire()
        self.guessRoom.append(user)
        print 'Join guess Room with ' + str(len(self.guessRoom)) + ' people'
        user.conn.send('There are ' + str(len(self.guessRoom)) + ' people in the room\n')
        if (len(self.guessRoom) >= guessUserLimit):
            print 'Start guess number!'
            r = GameRoom.GuessRoom(self.guessRoom, self)
            r.daemon = True
            r.start()
            self.guessRoom = []
        self.guessLock.release()
    def add2BjackRoom(self, user):
        self.bjackLock.acquire()
        self.bjackRoom.append(user)
        print 'Join blackjack Room with ' + str(len(self.bjackRoom)) + ' people'
        user.conn.send('There are ' + str(len(self.bjackRoom)) + ' people in the room\n')
        if (len(self.bjackRoom) >= bjackUserLimit):
            print 'Start blackjack number!'
            r = GameRoom.BjackRoom(self.bjackRoom, self)
            r.daemon = True
            r.start()
            self.bjackRoom = []
        self.bjackLock.release()

    def addConnection(self, user):
        time.sleep(0.1)
        failTime = 0
        while True:
            tp = recvline(user.conn, 1000)
            if tp == '1': # guess number
                self.add2GuessRoom(user)
                return
            elif tp == '2':         # blackjack
                self.add2BjackRoom(user)
                return
            elif tp == 'q':
                break
            failTime += 1
            if failTime > 40: break
        user.conn.close()
        print 'close the client'
    def run(self):
        print 'HOST : ' + self.host
        print 'PORT : ' + str(self.port)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 6)
        try:
            s.bind((self.host, self.port))
            s.listen(10)
            while True:
                conn, addr = s.accept()
                print 'Connected with ' + addr[0] + ':' + str(addr[1])
                user = User.User(conn)
                t = threading.Thread(target=self.addConnection, args=(user,))
                t.daemon = True
                t.start()
        except socket.error as msg:
            print 'Bind failed ' + str(msg[0]) + ' : ' + msg[1]
            sys.exit()


