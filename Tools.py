import select, random, json
import thread

def recvline(conn, sec):
    try:
        ret = ''
        conn.setblocking(0)
        while True:
            ready = select.select([conn], [], [], sec)
            if ready[0]:
                tmp = conn.recv(1)
                if tmp == '\n' or tmp == '': break
                ret = ret + tmp
            else:
                print 'Disconnect'
                break
        return ret.strip()
    except:
        print 'Socket error'
        return ''

def sendline(conn, s):
    try:
        conn.send(s + '\n')
    except:
        print 'QAQ'
        pass
#        raise Exception('Disconnected')
def sendJson(conn, data):
    try:
        s = json.dumps(data) + '\n'
    except (TypeError, ValueError), e:
        raise Exception('You can only send JSON-serializable data')
    sendline(conn, str(len(s)))
    try:
        conn.sendall(s)
    except:
        print 'send failed'
        pass
def recvJson(conn):
        ret = ''
        l = int(recvline(conn, 10000))
        offset = 0
        while l - offset > 0:
            tmp = conn.recv(l - offset)
            ret += tmp
            offset += len(tmp)
        print ret
        return json.loads(ret)

def value(x):
    if x >= 10: return 10
    return x

def sum(x):
    ret = 0
    x.sort()
    for i in x[::-1]:
        ret += value(i)
    A = x.count(1)
    for i in range(A):
        tmp = ret - 1 + 11
        if tmp <= 21: ret = tmp
    if ret > 21: ret = -1
    if ret == 21 and len(x) == 2: ret = 22
    return ret

def judgeBlackjack(a, b):
    x = sum(a)
    y = sum(b)
    if y < 0: return 'Serv'
    if x == y: return 'Draw'
    return 'Serv' if x > y else 'User'

def getCard():
    return random.randint(1,13)

def getData(myID, uid, cid, servCard, users):
    data = [myID, uid, cid, servCard.cards]
    u = []
    for it in users:
        tmp = []
        for c in it.cards:
            tmp.append(c.cards)
        u.append(tmp)
    data.append(u)
    return data

